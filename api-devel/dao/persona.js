/**
 * New node file
 */

module.exports = function (db) {
    var W = require('when');
    var dao = {};

    dao.getById = function (id, t) {
        var df = W.defer();
        db.Persona.find({where: { id: id}, transaction: t}).success(df.resolve).error(df.reject);
        return df.promise;
    }
    return dao;
}