/**
 * New node file
 */

module.exports = function(db) {

	var dao = {};
	var W = require('when');
    var util = require('../util');


    dao.create = function (filmId,userId,opinion, t) {
        var df = W.defer();
        var options = util.addTrans(t, {});
        db.Opinion.create({opinion:opinion,PeliculaId:filmId,UserId:userId}, options).success(df.resolve).error(df.reject);
        return df.promise;
    }

    dao.getOpinion = function (opinionId, t) {
        var df = W.defer();
        db.Opinion.find({
            where: {id: opinionId},
            transaction: t
        }).success(df.resolve).error(df.reject);
        return df.promise;
    },

        dao.listOpinions = function (filmID,t) {
            var df = W.defer();
            db.Opinion.findAll(
                {where: {PeliculaId: filmID},
                    attributes:['id','opinion'],
                    include: [{model: db.User, attributes: ['name']}],
                    order : 'id DESC'}
                //{order : 'createdAt DESC'}
            ).success(df.resolve).error(df.reject);
            return df.promise;
        }

    dao.listOpinionsByUserId = function (userId,t) {
        var df = W.defer();
        db.Opinion.findAll(
            {where: {UserId: userId},
                attributes:['id','opinion'],
                include: [{model: db.Pelicula, attributes: ['titol']}],
                order : 'id DESC'}
            //{order : 'createdAt DESC'}
        ).success(df.resolve).error(df.reject);
        return df.promise;
    }

	return dao;
}