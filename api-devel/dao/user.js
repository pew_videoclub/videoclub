/**
 * New node file
 */

module.exports = function (db) {
    var W = require('when');
    var dao = {};
    var util = require('../util');

    dao.getById = function (id, t) {
        var df = W.defer();
        db.User.find({where:{id: id},attributes:['id','name','surname'], transaction: t}).success(df.resolve).error(df.reject);
        return df.promise;
    }

    dao.getByEmail = function (email, t) {
        var df = W.defer();
        db.User.find({where: { email: email}, transaction: t}).success(df.resolve).error(df.reject);
        return df.promise;
    }

    dao.create = function (user, t) {
        var df = W.defer();
        var options = util.addTrans(t, {});
        db.User.create(user, options).success(df.resolve).error(df.reject);
        return df.promise;
    }

    return dao;
}