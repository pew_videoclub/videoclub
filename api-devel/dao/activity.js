/**
 * New node file
 */

module.exports = function(db) {

	var dao = {};
	var W = require('when');
    var util = require('../util');


    dao.create = function (message,userId, filmId, t) {
        var df = W.defer();
        var options = util.addTrans(t, {});
        db.Activity.create({description:message,UserId:userId,PeliculaId:filmId}, options).success(df.resolve).error(df.reject);
        return df.promise;
    }

    dao.getActivity = function (activityId, t) {
        var df = W.defer();
        db.Activity.find({
            where: {id: activityId},
            transaction: t
        }).success(df.resolve).error(df.reject);
        return df.promise;
    },

    dao.listActivity = function (userID,t) {
        var df = W.defer();
        db.Activity.findAll(
            {where: {UserId: userID},
            include: db.Pelicula,
            order : 'id DESC'}
        ).success(df.resolve).error(df.reject);
        return df.promise;
    }

	return dao;
}