/**
 * New node file
 */

module.exports = function (db) {
    var W = require('when');
    var dao = {};

    dao.getById = function (id, t) {
        var df = W.defer();
        db.Pelicula.find({
            where: { id: id},
            transaction: t
        }).success(df.resolve).error(df.reject);
        return df.promise;
    },

    dao.search = function (toSearch, t) {
        var df = W.defer();
        db.Pelicula.findAll({
            where: {titol: {like: '%' + toSearch + '%'}},
            limit: 24
            //sort: [updatedAt, descending]
        }).success(df.resolve).error(df.reject);
        return df.promise;
    },

    dao.topVistes = function (t) {
        var df = W.defer();
        db.Pelicula.findAll({
            limit: 24,
            order : 'id DESC'
        }).success(df.resolve).error(df.reject);
        return df.promise;
    },

    dao.topBucades = function (t) {
        var df = W.defer();
        db.Pelicula.findAll({
            limit: 24,
            order : 'id ASC'}
        ).success(df.resolve).error(df.reject);
        return df.promise;
    }

    return dao;
}