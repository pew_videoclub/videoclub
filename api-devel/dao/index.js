/**
 * New node file
 */

module.exports = function(db) {
	var dao = {};
	var W = require('when');

	dao.Opinion = require('./opinion')(db);
    //dao.Client = require('./client')(db);
    dao.User = require('./user')(db);
    dao.Pelicula = require('./pelicula')(db);
    dao.Persona = require('./persona')(db);
    dao.Productora = require('./productora')(db);
    dao.RelUserFilm = require('./rel_user_film')(db)
    dao.Activity = require('./activity')(db)

    dao.create = function(Model, data) {
		var df = W.defer();
		Model.create(data).success(df.resolve).error(df.reject);
		return df.promise;
	}

	return dao;
}