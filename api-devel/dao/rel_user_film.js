/**
 * New node file
 */

module.exports = function(db) {

	var dao = {};
	var W = require('when');
    var util = require('../util');


    dao.getRating = function (filmID,userID, t) {
        var df = W.defer();
        db.RelUserFilm.find({
            where: {PeliculaId: filmID, UserId: userID},
            attributes:['id','nota'],
            transaction: t
        }).success(df.resolve).error(df.reject);
        return df.promise;
    }

    dao.getNotaMitja = function (filmID, t) {
        var df = W.defer();
        /*
        sequalize.query('SELECT avg(r.nota) FROM RelUserFilm r WHERE r.PeliculaId = :peliculaId AND r.nota IS NOT NULL"', null,
            { raw: true },
            { peliculaId: filmID }
        ).success(df.resolve).error(df.reject);
        return df.promise;

*/

        db.RelUserFilm.findAll({
            where: ["PeliculaId = ? AND nota IS NOT NULL", filmID],
            transaction: t
        }).success(df.resolve).error(df.reject);
        return df.promise;

    }

    dao.getVista = function (filmID,userID, nota, t) {
        var df = W.defer();
        db.RelUserFilm.find({
            where: {PeliculaId: filmID, UserId: userID},
            attributes:['id','vista'],
            transaction: t
        }).success(df.resolve).error(df.reject);
        return df.promise;
    }

    dao.getFavorita = function (filmID,userID, nota, t) {
        var df = W.defer();
        db.RelUserFilm.find({
            where: {PeliculaId: filmID, UserId: userID},
            attributes:['id','favorita'],
            transaction: t
        }).success(df.resolve).error(df.reject);
        return df.promise;
    }

    dao.create = function (filmID,userID, t) {
        var df = W.defer();
        db.RelUserFilm.create({nota:'0',favorita:'0',vista:'0',PeliculaId:filmID,UserId:userID}
        ).success(df.resolve).error(df.reject);
        return df.promise;
    }

    dao.getFavorites = function(userID,t) {
        var df = W.defer();
        db.RelUserFilm.findAll({
            where : {UserId:userID, favorita:1},
            order : 'createdAt DESC',
            include: db.Pelicula
        }).success(df.resolve).error(df.reject);
        return df.promise;
    }

    dao.getVieweds = function(userID,t) {
        var df = W.defer();
        db.RelUserFilm.findAll({
            where : {UserId:userID, vista:1},
            order : 'createdAt DESC',
            include: db.Pelicula
        }).success(df.resolve).error(df.reject);
        return df.promise;
    }
	return dao;
}