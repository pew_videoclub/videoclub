INSERT INTO `apidb`.`Users` VALUES (1,'Prova','Patata',NULL,NULL,NULL,NULL,NULL,'Prova@Prova.com','Prova','2014-05-15 15:08:17','2014-05-15 15:08:17');
INSERT INTO `apidb`.`Users` VALUES (2,'Prova1','Patata',NULL,NULL,NULL,NULL,NULL,'Prova1@Prova.com','Prova1','2014-05-15 15:08:17','2014-05-15 15:08:17');

INSERT INTO `apidb`.`Peliculas` (`titol`, `titol_original`, `cartell`, `createdAt`, `updatedAt`, `sinapsi`)
VALUES ('Pulp Fiction', 'Pulp Fiction', 'pulpfiction.jpg', '2014-05-12 20:24:52', '2014-05-12 20:24:52',
 'Thomas Anderson es un brillante programador de una respetable compañía de software. Pero fuera del trabajo es Neo, un hacker que un día recibe una misteriosa visita...' );

INSERT INTO `apidb`.`Peliculas` (`titol`, `titol_original`, `cartell`, `createdAt`, `updatedAt`, `sinapsi`)
VALUES ('Django desencadenado', 'Djando Unchained', 'djangounchained.jpg', '2014-05-12 20:24:52', '2014-05-12 20:24:52',
 'En Texas, dos años antes de estallar la Guerra Civil Americana, King Schultz (Christoph Waltz), un cazarecompensas alemán que le sigue la pista a unos asesinos para cobrar por sus cabezas, le promete al esclavo negro Django (Jamie Foxx) dejarlo en libertad si le ayuda a atraparlos. Él acepta pues luego quiere ir a buscar a su esposa Broomhilda (Kerry Washington), una esclava que están en una plantación del terrateniente Calvin Candie (Leonardo DiCaprio).' );

INSERT INTO `apidb`.`Peliculas` (`titol`, `titol_original`, `cartell`, `createdAt`, `updatedAt`, `sinapsi`)
VALUES ('Matrix', 'Matrix', 'Matrix.jpg', '2014-05-12 20:24:52', '2014-05-12 20:24:52',
 'Thomas Anderson es un brillante programador de una respetable compañía de software. Pero fuera del trabajo es Neo, un hacker que un día recibe una misteriosa visita...' );

  INSERT INTO `apidb`.`Peliculas` (`titol`, `titol_original`, `cartell`, `createdAt`, `updatedAt`, `sinapsi`)
VALUES ('12 Hombres sin piedad', '12 Angry Men', '12_hombres_sin_piedad.jpg', '2014-05-12 20:24:52', '2014-05-12 20:24:52',
 'Un grupo de miembros de un jurado recibe el encargo de juzgar a un adolescente acusado de haber matado a su padre. De los doce miembros, once están convencidos de que el acusado es culpable de asesinato. Pero el duodécimo no tiene ninguna duda sobre su inocencia. ¿Cómo puede este hombre convencer a los otros miembros sobre la inocencia del joven?' );

  INSERT INTO `apidb`.`Peliculas` (`titol`, `titol_original`, `cartell`, `createdAt`, `updatedAt`, `sinapsi`)
VALUES ('Cadena perpetua', 'The Shawshank Redemption', 'Cadena_perpetua.jpg', '2014-05-12 20:24:52', '2014-05-12 20:24:52',
 'Acusado del asesinato de su mujer, Andrew Dufresne (Tim Robbins), tras ser condenado a cadena perpetua, es enviado a la cárcel de Shawshank. Con el paso de los años conseguirá ganarse la confianza del director del centro y el respeto de sus compañeros de prisión, especialmente de Red (Morgan Freeman), el jefe de la mafia de los sobornos.' );


  INSERT INTO `apidb`.`Peliculas` (`titol`, `titol_original`, `cartell`, `createdAt`, `updatedAt`, `sinapsi`)
VALUES ('El apartamento', 'The Apartment', 'El_apartamento.jpg', '2014-05-12 20:24:52', '2014-05-12 20:24:52',
 'C.C. Baxter (Jack Lemmon) es un modesto pero ambicioso empleado de una compañía de seguros de Manhattan. Está soltero y vive solo en un discreto apartamento que presta ocasionalmente a sus superiores para sus citas amorosas. Tiene la esperanza de que estos favores le sirvan para mejorar su posición en la empresa. Pero la situación cambia cuando se enamora de una ascensorista (Shirley MacLaine) que resulta ser la amante de uno de los jefes que usan su apartamento (Fred MacMurray). ' );

  INSERT INTO `apidb`.`Peliculas` (`titol`, `titol_original`, `cartell`, `createdAt`, `updatedAt`, `sinapsi`)
VALUES ('El golpe', 'The Sting', 'El_golpe.jpg', '2014-05-12 20:24:52', '2014-05-12 20:24:52',
 'Chicago, años treinta. Redford y Newman son dos timadores que deciden vengar la muerte de un viejo y querido colega, asesinado por orden de un poderoso gángster (Robert Shaw). Para ello urdirán un ingenioso y complicado plan con la ayuda de todos sus amigos y conocidos.' );

  INSERT INTO `apidb`.`Peliculas` (`titol`, `titol_original`, `cartell`, `createdAt`, `updatedAt`, `sinapsi`)
VALUES ('El gran dictador', 'The Great Dictator', 'El_gran_dictador.jpg', '2014-05-12 20:24:52', '2014-05-12 20:24:52',
 'Un humilde barbero judío tiene un parecido asombroso con el dictador de Tomania, un tirano que culpa a los judíos de la crítica situación que atraviesa el país. Un día, sus propios guardias lo confunden con el barbero y lo llevan a un campo de concentración. Al mismo tiempo, al pobre barbero lo confunden con el tirano. ' );

  INSERT INTO `apidb`.`Peliculas` (`titol`, `titol_original`, `cartell`, `createdAt`, `updatedAt`, `sinapsi`)
VALUES ('El padrino', 'The Godfather', 'El_Padrino.jpg', '2014-05-12 20:24:52', '2014-05-12 20:24:52',
 'Años 40. Don Vito Corleone (Marlon Brando) es el respetado y temido jefe de una de las cinco familias de la mafia de Nueva York. Tiene cuatro hijos: una chica, Connie (Talia Shire), y tres varones: el impulsivo Sonny (James Caan), el pusilánime Freddie (John Cazale) y Michael (Al Pacino), que no quiere saber nada de los negocios de su padre. Cuando Corleone, siempre aconsejado por su consejero Tom Hagen (Robert Duvall), se niega a intervenir en el negocio de las drogas, el jefe de otra banda ordena su asesinato. Empieza entonces una violenta y cruenta guerra entre las familias mafiosas. ' );

  INSERT INTO `apidb`.`Peliculas` (`titol`, `titol_original`, `cartell`, `createdAt`, `updatedAt`, `sinapsi`)
VALUES ('El padrino II', 'The Godfather: Part II', 'El_Padrino_Parte_II.jpg', '2014-05-12 20:24:52', '2014-05-12 20:24:52',
 'Continuación de la saga de los Corleone con dos historias paralelas: la elección de Michael Corleone como jefe de los negocios familiares y los orígenes del patriarca, el ya fallecido Don Vito, primero en Sicilia y luego en Estados Unidos, donde, empezando desde abajo, llegó a ser un poderosísimo jefe de la mafia de Nueva York.' );

  INSERT INTO `apidb`.`Peliculas` (`titol`, `titol_original`, `cartell`, `createdAt`, `updatedAt`, `sinapsi`)
VALUES ('La lista de Schindler', 'Schindler''s List', 'La_lista_de_Schindler.jpg', '2014-05-12 20:24:52', '2014-05-12 20:24:52',
 'Segunda Guerra Mundial (1939-1945). Oskar Schindler (Liam Neeson), un hombre de enorme astucia y talento para las relaciones públicas, organiza un ambicioso plan para ganarse la simpatía de los nazis. Después de la invasión de Polonia por los alemanes (1939), consigue, gracias a sus relaciones con los nazis, la propiedad de una fábrica de Cracovia. Allí emplea a cientos de operarios judíos, cuya explotación le hace prosperar rápidamente. Su gerente (Ben Kingsley), también judío, es el verdadero director en la sombra, pues Schindler carece completamente de conocimientos para dirigir una empresa.' );

  INSERT INTO `apidb`.`Peliculas` (`titol`, `titol_original`, `cartell`, `createdAt`, `updatedAt`, `sinapsi`)
VALUES ('Luces de la ciudad', 'City Lights', 'Luces_de_la_ciudad.jpg', '2014-05-12 20:24:52', '2014-05-12 20:24:52',
 'Un pobre vagabundo (Charles Chaplin) pasa mil y un avatares para conseguir dinero y ayudar a una pobre chica ciega (Virginia Cherrill) de la que se ha enamorado. ' );

  INSERT INTO `apidb`.`Peliculas` (`titol`, `titol_original`, `cartell`, `createdAt`, `updatedAt`, `sinapsi`)
VALUES ('Testigo de cargo', 'Witness for the Prosecution', 'Testigo_de_cargo.jpg', '2014-05-12 20:24:52', '2014-05-12 20:24:52',
 'Leonard Vole (Tyrone Power), un hombre joven y atractivo, es acusado del asesinato de la señora French, una rica anciana con quien mantenía una relacion de carácter amistoso. El presunto móvil del crimen era la posibilidad de heredar los bienes de la difunta. A pesar de que las pruebas en su contra son demoledoras, Sir Wilfrid Roberts (Charles Laughton), un prestigioso abogado criminalista londinense, se hace cargo de su defensa.' );

INSERT INTO `apidb`.`RelUserFilms` (`id`, `nota`, `vista`, `favorita`, `createdAt`, `updatedAt`, `PeliculaId`, `UserId`)
VALUES (1, 5, 1, 1, '2014-05-15 15:08:17', '2014-05-15 15:08:17', 1, 1);

INSERT INTO `apidb`.`RelUserFilms` (`id`, `nota`, `vista`, `favorita`, `createdAt`, `updatedAt`, `PeliculaId`, `UserId`)
VALUES (4, 10, 1, 1, '2014-05-15 15:08:17', '2014-05-15 15:08:17', 1, 2);

INSERT INTO `apidb`.`RelUserFilms` (`id`, `nota`, `vista`, `favorita`, `createdAt`, `updatedAt`, `PeliculaId`, `UserId`)
VALUES (5, 7, 1, 1, '2014-05-15 15:08:17', '2014-05-15 15:08:17', 2, 2);

INSERT INTO `apidb`.`RelUserFilms` (`id`, `nota`, `vista`, `favorita`, `createdAt`, `updatedAt`, `PeliculaId`, `UserId`)
VALUES (6, 7, 1, 1, '2014-05-15 15:08:17', '2014-05-15 15:08:17', 3, 2);

INSERT INTO `apidb`.`RelUserFilms` (`id`, `nota`, `vista`, `favorita`, `createdAt`, `updatedAt`, `PeliculaId`, `UserId`)
VALUES (7, 4, 1, 1, '2014-05-15 15:08:17', '2014-05-15 15:08:17', 4, 2);

INSERT INTO `apidb`.`RelUserFilms` (`id`, `nota`, `vista`, `favorita`, `createdAt`, `updatedAt`, `PeliculaId`, `UserId`)
VALUES (8, 4, 1, 1, '2014-05-15 15:08:17', '2014-05-15 15:08:17', 5, 2);

INSERT INTO `apidb`.`RelUserFilms` (`id`, `nota`, `vista`, `favorita`, `createdAt`, `updatedAt`, `PeliculaId`, `UserId`)
VALUES (9, 4, 1, 1, '2014-05-15 15:08:17', '2014-05-15 15:08:17', 6, 2);

INSERT INTO `apidb`.`RelUserFilms` (`id`, `nota`, `vista`, `favorita`, `createdAt`, `updatedAt`, `PeliculaId`, `UserId`)
VALUES (10, 4, 1, 1, '2014-05-15 15:08:17', '2014-05-15 15:08:17', 7, 2);

INSERT INTO `apidb`.`RelUserFilms` (`id`, `nota`, `vista`, `favorita`, `createdAt`, `updatedAt`, `PeliculaId`, `UserId`)
VALUES (11, 4, 1, 1, '2014-05-15 15:08:17', '2014-05-15 15:08:17', 8, 2);

INSERT INTO `apidb`.`RelUserFilms` (`id`, `nota`, `vista`, `favorita`, `createdAt`, `updatedAt`, `PeliculaId`, `UserId`)
VALUES (12, 4, 1, 1, '2014-05-15 15:08:17', '2014-05-15 15:08:17', 9, 2);

INSERT INTO `apidb`.`RelUserFilms` (`id`, `nota`, `vista`, `favorita`, `createdAt`, `updatedAt`, `PeliculaId`, `UserId`)
VALUES (13, 4, 1, 1, '2014-05-15 15:08:17', '2014-05-15 15:08:17', 10, 2);

INSERT INTO `apidb`.`RelUserFilms` (`id`, `nota`, `vista`, `favorita`, `createdAt`, `updatedAt`, `PeliculaId`, `UserId`)
VALUES (14, 8, 0, 0, '2014-05-15 15:08:17', '2014-05-15 15:08:17', 11, 1);

INSERT INTO `apidb`.`RelUserFilms` (`id`, `nota`, `vista`, `favorita`, `createdAt`, `updatedAt`, `PeliculaId`, `UserId`)
VALUES (15, 4, 1, 1, '2014-05-15 15:08:17', '2014-05-15 15:08:17', 12, 2);

INSERT INTO `apidb`.`RelUserFilms` (`id`, `nota`, `vista`, `favorita`, `createdAt`, `updatedAt`, `PeliculaId`, `UserId`)
VALUES (3, 2, 1, 0, '2014-05-15 15:08:17', '2014-05-15 15:08:17', 3, 1);

INSERT INTO `apidb`.`Opinions` (`id`, `opinion`, `createdAt`, `updatedAt`, `PeliculaId`, `UserId`)
VALUES (1, 'Creo que es una pelicula nefasta', '2014-05-15 15:08:17', '2014-05-15 15:08:17', 1, 1);

INSERT INTO `apidb`.`Opinions` (`id`, `opinion`, `createdAt`, `updatedAt`, `PeliculaId`, `UserId`)
VALUES (2, 'Jo no entenc el film...', '2014-05-15 15:08:17', '2014-05-15 15:08:17', 2, 1);

INSERT INTO `apidb`.`Opinions` (`id`, `opinion`, `createdAt`, `updatedAt`, `PeliculaId`, `UserId`)
VALUES (3, 'Jo no entenc el film...', '2014-05-15 15:08:17', '2014-05-15 15:08:17', 2, 2);

INSERT INTO `apidb`.`Activities` (`id`, `description`, `createdAt`, `updatedAt`, `UserId`, `PeliculaId`)
VALUES (1, 'Ha puntuat la pelicula Pulp Fiction amb un 5', '2014-05-15 15:08:17', '2014-05-15 15:08:17', 1, 1);

INSERT INTO `apidb`.`Activities` (`id`, `description`, `createdAt`, `updatedAt`, `UserId`, `PeliculaId`)
VALUES (2, 'Ha puntuat la pelicula Pulp Fiction amb un 10', '2014-05-15 15:08:17', '2014-05-15 15:08:17', 2, 1);

INSERT INTO `apidb`.`Activities` (`id`, `description`, `createdAt`, `updatedAt`, `UserId`, `PeliculaId`)
VALUES (3, 'Ha puntuat la pelicula Django desencadenado amb un 8', '2014-05-15 15:08:17', '2014-05-15 15:08:17', 1, 2);

INSERT INTO `apidb`.`Activities` (`id`, `description`, `createdAt`, `updatedAt`, `UserId`, `PeliculaId`)
VALUES (8, 'Ha comentat la pelicula Django desencadenado', '2014-05-15 15:08:17', '2014-05-15 15:08:17', 1, 2);

INSERT INTO `apidb`.`Activities` (`id`, `description`, `createdAt`, `updatedAt`, `UserId`, `PeliculaId`)
VALUES (9, 'Ha comentat la pelicula Django desencadenado', '2014-05-15 15:08:17', '2014-05-15 15:08:17', 2, 2);

INSERT INTO `apidb`.`Activities` (`id`, `description`, `createdAt`, `updatedAt`, `UserId`, `PeliculaId`)
VALUES (4, 'Ha puntuat la pelicula Matrix amb un 2', '2014-05-15 15:08:17', '2014-05-15 15:08:17', 1, 3);

INSERT INTO `apidb`.`Activities` (`id`, `description`, `createdAt`, `updatedAt`, `UserId`, `PeliculaId`)
VALUES (5, 'Ha marcat com vista Pulp Fiction', '2014-05-15 15:08:17', '2014-05-15 15:08:17', 1, 1);

INSERT INTO `apidb`.`Activities` (`id`, `description`, `createdAt`, `updatedAt`, `UserId`, `PeliculaId`)
VALUES (6, 'Ha marcat com favorita Pulp Fiction', '2014-05-15 15:08:17', '2014-05-15 15:08:17', 1, 1);

INSERT INTO `apidb`.`Activities` (`id`, `description`, `createdAt`, `updatedAt`, `UserId`, `PeliculaId`)
VALUES (7, 'Ha marcat com vista Matrix', '2014-05-15 15:08:17', '2014-05-15 15:08:17', 1, 3);