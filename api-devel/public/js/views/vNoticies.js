/**
 * Created by marc on 23/05/14.
 */

define([
    'jquery',
    'underscore',
    'backbone',
    'bootstrap',
    'text!../../templates/noticies/tNoticies.html',
    'views/noticies/vNoticies',
    'views/vVar'
], function($, _, Backbone,Bootstrap,tNoticies,vNoticies,vVar){

    var view = Backbone.View.extend({

        el: $('#contextApp'),

        events: {
        },

        initialize : function(){
            $('#varApp').html("");
            this.$el.html("");
            this.render();
        },

        render: function(){

            var _vVar = new vVar();
            this.$el.append( _.template( tNoticies, {} ) );

            var noticies = new vNoticies({el : this.$el.find('#IdDivNoticies')});
        }

    });

    return view;

});
