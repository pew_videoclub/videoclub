define([
    'jquery',
    'underscore',
    'backbone',
    'bootstrap',
    'text!../../templates/login/tLogin.html',
    'text!../../templates/login/tRegistrar.html'
], function($, _, Backbone,Bootstrap,tLogin,tRegistrar){

    var Login = Backbone.View.extend({

        el: $('#contextApp'),

        events: {
             "click #btnsubmitRegistrar" : "registrar",
             "click #btnsubmitLogin" : "login"
        },

        initialize : function(){
            $('#varApp').html("");
            $('#contextApp').html("");
            this.render();
        },

        render: function(){
            $('#varApp').html("");
            $('#contextApp').html("");

            this.$el.append( _.template( tLogin, {} ) );
            this.$el.append( _.template( tRegistrar, {} ) );
        },

        login : function(){

            var self = this;
            var json = $.ajax({
                url : "/login",
                type : "POST",
                dataType : "json",
                data : {
                    mail: self.$el.find('#inputEmail_log').val(),
                    pwd: self.$el.find('#inputPassword_log').val()
                }
            }).done(function( response, status, jqXHR ) {

                window.location.href = "/#home";

            }).fail(function(response) {
                $("#msgError_log").text("Error: " + response.responseJSON.error);
            });
        },

        registrar : function(){

            var self = this;
            var json = $.ajax({
                url : "/user",
                type : "POST",
                dataType : "json",
                data : {
                    name: self.$el.find('#inputName_reg').val(),
                    cognom: self.$el.find('#inputSecondName_reg').val(),
                    mail: self.$el.find('#inputEmail_reg').val(),
                    pass1: self.$el.find('#inputPassword_reg').val(),
                    pass2: self.$el.find('#inputPassword2_reg').val()
                }
            }).done(function( response, status, jqXHR ) {

                window.location.href = "/#home";

            }).fail(function(response) {
                $("#msgError_reg").text("Error: " + response.responseJSON.error );
            });
        }

    });

    return Login;

});