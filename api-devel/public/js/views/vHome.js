/**
 * Created by marc on 24/04/14.
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'bootstrap',
    'text!../../templates/home/thome.html',
    'views/vVar',
    'views/home/vPeliculesMesVistes',
    'views/home/vPeliculesMesBuscades'
], function($, _, Backbone,Bootstrap,thome,vVar,vPeliculesMesVistes,vPeliculesMesBuscades){

    var view = Backbone.View.extend({

        el: $('#contextApp'),

        events: {
        },

        initialize : function(){
            $('#varApp').html("");
            this.$el.html("");
            this.render();
        },

        render: function(){

            var _vVar = new vVar();
            this.$el.append( _.template( thome, {} ) );

            var vpeliculesMesVistes = new vPeliculesMesVistes({
                el : this.$el.find('#divDeLesMesVistes')})

            var vpeliculesMesBuscades = new vPeliculesMesBuscades({
                el : this.$el.find('#divDeLesMesBuscades')})
        }

    });

    return view;

});
