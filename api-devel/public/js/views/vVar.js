/**
 * Created by marc on 07/05/14.
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'bootstrap',
    'text!../../templates/var/tVar.html'
], function($, _, Backbone,Bootstrap,template){

    var vHome = Backbone.View.extend({

        el: $('#varApp'),

        events: {
            "click #idlogout" : "logout"
        },

        initialize : function(){
            $('#varApp').html("");
            this.$el.html("");
            this.render();

        },

        render: function(){
            var self = this;
            this.$el.append( _.template( template, {} ) );

            var self = this;
            var json = $.ajax({
                url : "/user/auth",
                type : "GET",
                dataType : "json"
            }).done(function( response, status, jqXHR ) {

                self.$el.find('#IdPerfilUsuariVar').attr("href", '#perfil/'+response.id);

            }).fail(function(response) {
            });

            this.$el.find('#inputVarBuscar').keypress(function(event){
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if(keycode == '13'){
                    self.buscar();
                }
            });

            this.$el.find('#btnVarBuscar').click(function(){
                self.buscar();
            });

        },

        buscar : function(){
            var self = this;
            var input = self.$el.find('#inputVarBuscar');
            window.location.href = "/#search/"+input.val();
        },

        logout : function(){
            var json = $.ajax({
                url : "/logout",
                type : "POST",
                dataType : "json"
            }).done(function( response, status, jqXHR ) {
                window.location.href = "/#login";

            }).fail(function(response) {
            });
        }

    });

    return vHome;

});
