/**
 * Created by marc on 24/04/14.
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'bootstrap',
    'text!../../templates/search/tSearch.html',
    'views/vVar',
    'views/home/vItemFilm'
], function($, _, Backbone,Bootstrap,tSearch,vVar,vItemFilm){

    var view = Backbone.View.extend({

        el: $('#contextApp'),

        events: {
        },

        initialize : function(){

        },

        render: function(text){
            var self = this;
            $('#varApp').html("");
            this.$el.html("");
            var _vVar = new vVar();

            this.$el.append( _.template( tSearch, {} ) );

            var json = $.ajax({
                url : "/pelicula/search/"+text,
                type : "GET",
                dataType : "json"
            }).done(function( response, status, jqXHR ) {

                self.printList(response);

            }).fail(function(response) {
                alert("Error al mostrar la pelicula.");
            });
        },

        printList : function(llista){

            var MAX = 6;
            for (var i in llista){
                if(i < MAX){
                    this.printItem(llista[i]);
                }
            }
        },

        printItem : function(item){

            this.$el.find('#divResultatDelBuscador').append('<div class="col-md-2 col-sm-3 col-xs-4">' +
                '</div>');

            var film = new vItemFilm({el : this.$el.find('div:last'), model : item});

        }

    });

    return view;

});
