/**
 * Created by Gabri on 23/05/2014.
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'bootstrap',
], function($, _, Backbone,Bootstrap,tNoticies,vVar){

    var view = Backbone.View.extend({

        events: {
        },

        initialize : function(){
            this.render();
        },

        render: function(){
            this.$el.html("");

            this.$el.addClass('noticia');
            this.$el.append('<div class="col-md-12">' +
                this.model.description +
                '</div>' +
                '<div class="col-md-12"> <small>' +
                this.model.date +
                '</small></div>');
        }

    });

    return view;

});
