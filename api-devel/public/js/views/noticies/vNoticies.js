/**
 * Created by Gabri on 23/05/2014.
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'bootstrap',
    'views/noticies/vItemNoticia'
], function($, _, Backbone,Bootstrap,vItemNoticia){

    var view = Backbone.View.extend({

        events: {
        },

        initialize : function(){
            this.render();
        },

        render: function(){
            this.$el.html("");

            var list = [{description: "....",date: "1/1/2001"},
                {description: "....",date: "1/1/2001"},
                {description: "....",date: "1/1/2001"},
                {description: "....",date: "1/1/2001"},
                {description: "....",date: "1/1/2001"},
                {description: "....",date: "1/1/2001"},
                {description: "....",date: "1/1/2001"},
                {description: "....",date: "1/1/2001"}];

            this.printList(list);
        },

        printList : function(list){

            if(list == null){return;}
            for(var i in list){
                this.printItem(list[i]);
            }
        },
        printItem : function(item){

            if(item == null){return;}

            this.$el.append('<div class="col-md-12"></div>');
            var _div = this.$el.find('div:last');

            var noticia = new vItemNoticia({el: _div, model: item});

        }

    });

    return view;

});
