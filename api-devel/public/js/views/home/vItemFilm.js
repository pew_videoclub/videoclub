/**
 * Created by marc on 09/05/14.
 */
/**
 * Created by marc on 07/05/14.
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'bootstrap'
], function($, _, Backbone,Bootstrap,thome,vVar){

    var view = Backbone.View.extend({


        events: {
        },

        initialize : function(){
            this.$el.html("");
            this.render();
        },

        render: function(){

            var item = this.model;
            //var src =  "data:image/png;base64,";//+item.src;
            var src =  "img/films/" + item.cartell;


            this.$el.append('<img class="classImg" src="'+src+'">');

            var img = this.$el.find('img:last');

            img.click(function(){
                window.location.href = '/#film/'+item.id;
            });
        }

    });

    return view;

});