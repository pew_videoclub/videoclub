/**
 * Created by marc on 07/05/14.
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'bootstrap',
    'views/home/vItemFilm'
], function($, _, Backbone,Bootstrap,vItemFilm){

    var view = Backbone.View.extend({


        events: {
        },

        initialize : function(){
            this.$el.html("");
            this.render();
        },

        render: function(){

            var self = this;
            var json = $.ajax({
                url : "/pelicula/top/buscades",
                type : "GET",
                dataType : "json"
            }).done(function( response, status, jqXHR ) {

                self.printList(response);

            }).fail(function(response) {
                alert("Error Top Vistes");
            });

        },


        printList : function(llista){

            var MAX = 6;
            for (var i in llista){
                if(i < MAX){
                    this.printItem(llista[i]);
                }
            }

        },

        printItem : function(item){

            this.$el.append('<div class="col-md-2 col-sm-3 col-xs-4 itemFilm">' +
                '</div>');

            var film = new vItemFilm({el : this.$el.find('div:last'), model : item});

        }

    });

    return view;

});