/**
 * Created by marc on 12/6/14.
 */

define([
    'jquery',
    'underscore',
    'backbone',
    'bootstrap'
], function($, _, Backbone,Bootstrap){

    var view = Backbone.View.extend({


        events: {
        },

        initialize : function(){
        },

        render: function(filmId){
            this.$el.html("");
            var self = this;

            var json = $.ajax({
                url : "/pelicula/notaMitja/"+filmId,
                type : "GET",
                dataType : "json"
            }).done(function( response, status, jqXHR ) {

                if(response.nota){

                    self.setNotaMitja(response.nota);
                }


            }).fail(function(response) {
            });

        },


        setNotaMitja : function(notaMitja){

            this.$el.append(notaMitja);

        }

    });

    return view;

});