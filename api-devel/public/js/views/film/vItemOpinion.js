/**
 * Created by marc on 30/05/14.
 */

define([
    'jquery',
    'underscore',
    'backbone',
    'bootstrap'
], function($, _, Backbone,Bootstrap,thome,vVar){

    var view = Backbone.View.extend({


        events: {
        },

        initialize : function(){
            this.$el.html("");
        },

        render: function(){

            var item = this.model;

            this.$el.append('<div> Opinió de <strong> <a href="#perfil/'+item.user.id+'">'+ item.user.name+'</a></strong>:</div>' +
                '<div>'+ item.opinion+'</div>');
           /*
            this.$el.append('<div>' + this.model.titol + '</div><img class="classImg" src="'+src+'">' +
                '<small>'+item.id+' usuaris</small>');
           */
        }

    });

    return view;

});