/**
 * Created by marc on 30/05/14.
 */

define([
    'jquery',
    'underscore',
    'backbone',
    'bootstrap',
    'text!../../../templates/film/tCrearOpinio.html',
    'views/film/vOpinions'
], function($, _, Backbone,Bootstrap,tCrearOpinio,vOpinions){

    var view = Backbone.View.extend({

        el: $('#modelApp'),

        events: {
        },

        initialize : function(){
            this.$el.html("");
            this.$el.append( _.template( tCrearOpinio, {}));
        },

        render: function(filmId){

            var self = this;

            this.$el.modal();
            this.model = filmId;
            this.$el.find("#textareaOpinio").focus();
            this.$el.find('#saveOpinion').click(function(){
                 self.saveOpinion();
            });

        },

        saveOpinion : function(){
            var self = this;
            var opinion = this.$el.find("#textareaOpinio").val();

            if(opinion == null || opinion == ""){
                this.$el.find("#textareaOpinio").addClass('red');
                return;
            }
            var filmId = this.model;
            var self = this;
            var json = $.ajax({
                url : "/pelicula/"+filmId+"/opinion",
                type : "POST",
                dataType : "json",
                data : {opinion : opinion}
            }).done(function( response, status, jqXHR ) {

                if(response){

                    self.$el.find("#textareaOpinio").removeClass('red');
                    self.$el.modal('hide');

                }else{
                    self.$el.find("#textareaOpinio").addClass('red');
                }

                var _divOpinions = $('.opinions').eq(0);
                var opinions = new vOpinions({el : _divOpinions});
                opinions.render(filmId);


            }).fail(function(response) {
            });


        }


    });

    return view;

});