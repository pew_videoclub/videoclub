/**
 * Created by marc on 23/05/14.
 */

define([
    'jquery',
    'underscore',
    'backbone',
    'bootstrap'
], function($, _, Backbone,Bootstrap){

    var view = Backbone.View.extend({


        events: {
        },

        initialize : function(){
        },

        render: function(filmId){
            this.$el.html("");
            var self = this;

            var json = $.ajax({
                url : "/pelicula/vista/"+filmId,
                type : "GET",
                dataType : "json"
            }).done(function( response, status, jqXHR ) {

                if(response.vista){

                    self.btnFav(filmId);
                }else{

                    self.btnNoFav(filmId);
                }


            }).fail(function(response) {
                self.btnNoFav(filmId);
            });
        },


        btnFav : function(filmId){
            var self = this;

            this.$el.append('<button type="button" class="btnNormal btn btn-info" >Vista</button>');

            var btn = this.$el.find('button:last');

            btn.hover(function(){

                $(this).html("No Vista");
                $(this).removeClass('btn-info');
                $(this).addClass('btn-warning')


            },function(){

                $(this).html("Vista");
                $(this).removeClass('btn-warning');
                $(this).addClass('btn-info');

            });

            btn.click(function(){
                self.putFav(filmId,false);
            });

        },

        btnNoFav : function(filmId){
            var self = this;

            this.$el.append('<button type="button" class="btnNormal btn btn-warning" >No Vista</button>');

            var btn = this.$el.find('button:last');

            btn.hover(function(){

                $(this).html("Vista");
                $(this).removeClass('btn-warning');
                $(this).addClass('btn-info');

            },function(){

                $(this).html("No Vista");
                $(this).removeClass('btn-info');
                $(this).addClass('btn-warning')

            });

            btn.click(function(){
                self.putFav(filmId,true);
            });
        },


        putFav : function(filmId,vista){
            var self = this;

            if(filmId == null || vista == null){
                return;
            }

            if(vista){
                var json = $.ajax({
                    url : "/pelicula/vista/"+filmId,
                    type : "POST",
                    dataType : "json",
                    data : {vista : vista}
                }).done(function( response, status, jqXHR ) {
                    self.render(filmId);

                }).fail(function(response) {
                    self.render(filmId);
                });
            }else{
                var json = $.ajax({
                    url : "/pelicula/vista/"+filmId,
                    type : "PUT",
                    dataType : "json",
                    data : {vista : vista}
                }).done(function( response, status, jqXHR ) {
                    self.render(filmId);

                }).fail(function(response) {
                    self.render(filmId);
                });


            }

        }

    });

    return view;

});