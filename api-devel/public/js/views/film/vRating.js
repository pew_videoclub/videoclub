/**
 * Created by marc on 16/05/14.
 */

define([
    'jquery',
    'underscore',
    'backbone',
    'bootstrap'
], function($, _, Backbone,Bootstrap){

    var view = Backbone.View.extend({


        events: {
        },

        initialize : function(){
        },

        render: function(filmId){
            var self = this;

            var json = $.ajax({
                url : "/pelicula/rating/"+filmId,
                type : "GET",
                dataType : "json"
            }).done(function( response, status, jqXHR ) {

                if(response == null){
                    self.model = null;
                    self.hoverNotaFull(0);
                }else{
                    self.model = response;
                    self.hoverNotaFull(self.model.nota);
                }

            }).fail(function(response) {
                self.model = null;
                self.hoverNotaFull(0);
            });


            self.options(filmId);

        },

        options : function(filmId){
            var self = this;

            this.$el.find('span').each(function( index ){
                self.putHover($(this),index);
                self.putClick($(this),index,filmId);
            });
        },

        putHover : function(star,nstar){
            var self = this;
            star.hover(function(){
                self.hoverNotaFull(nstar+1);
            },function(){
                if(self.model != null){
                    self.hoverNotaFull(self.model.nota);

                }else{
                    self.hoverNotaFull(0);

                }

            });
        },

        putClick : function(star,nstar,filmId){
            var self = this;

            star.click(function(){

                var json = $.ajax({
                    url : "/pelicula/rating/"+filmId,
                    type : "PUT",
                    dataType : "json",
                    data : {
                        nota : nstar+1
                    }
                }).done(function( response, status, jqXHR ) {

                    if(response == null){
                        self.model = null;
                        self.hoverNotaFull(0);
                    }else{
                        self.model = response;
                        self.hoverNotaFull(self.model.nota);
                    }

                }).fail(function(response) {
                    self.model = null;
                    self.hoverNotaFull(0);
                });
            });
        },

        hoverNotaFull : function(nota){

            this.$el.find('span').each(function(index){
                if(index < nota){
                    $(this).removeClass('glyphicon-star-empty');
                    $(this).addClass('glyphicon-star');
                }else{
                    $(this).removeClass('glyphicon-star');
                    $(this).addClass('glyphicon-star-empty');
                }
            });
        }

    });

    return view;

});