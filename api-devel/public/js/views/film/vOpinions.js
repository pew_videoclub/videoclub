/**
 * Created by marc on 30/05/14.
 */

define([
    'jquery',
    'underscore',
    'backbone',
    'bootstrap',
    'views/film/vItemOpinion'
], function($, _, Backbone,Bootstrap,vItemOpinion){

    var view = Backbone.View.extend({


        events: {
        },

        initialize : function(){
        },

        render: function(filmId){
            this.$el.html("");
            var self = this;

            var json = $.ajax({
                url : "/pelicula/"+filmId+"/getOpinions",
                type : "GET",
                dataType : "json"
            }).done(function( response, status, jqXHR ) {

                if(response){

                    self.printList(response);

                }else{

                    //TODO:
                }


            }).fail(function(response) {
                //TODO:
            });

        },


        printList : function(llista){

            for (var i in llista){

                    this.printItem(llista[i]);
            }

        },

        printItem : function(opinio){

            this.$el.append('<div class="opinion"></div>');
            var _div = this.$el.find('.opinion:last');
            var opinion = new vItemOpinion({el : _div, model : opinio});
            opinion.render();

        }


    });

    return view;

});