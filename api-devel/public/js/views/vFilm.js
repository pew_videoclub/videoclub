/**
 * Created by marc on 24/04/14.
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'bootstrap',
    'text!../../templates/film/tFilm.html',
    'views/vVar',
    'views/film/vRating',
    'views/film/vFavorita',
    'views/film/vOpinions',
    'views/film/vVista',
    'views/film/vCrearOpinio',
    'views/film/vNotaMitja'
], function($, _, Backbone,Bootstrap,tFilm,vVar,vRating,vFavorita,vOpinions,vVista,vCrearOpinio,vNotaMitja){

    var view = Backbone.View.extend({

        el: $('#contextApp'),

        events: {
            "click #idCrearOpinio" : "createOpinion",
            "click #saveOpinion" : "saveOpinion"
        },

        initialize : function(){
            $('#varApp').html("");
            this.$el.html("");
        },

        render: function(filmId){

            this.model = filmId;
            $('#varApp').html("");
            this.$el.html("");
            var self = this;

            var _vVar = new vVar();

            var json = $.ajax({
                url : "/pelicula/"+filmId,
                type : "GET",
                dataType : "json"
            }).done(function( response, status, jqXHR ) {

                self.$el.append( _.template( tFilm, {
                    titlefilm : response.titol,
                    titlefilmOriginal : response.titol_original,
                    sinapsi : response.sinapsi}));

                self.$el.find('#idImageFilm').attr('src','img/films/'+response.cartell);

                self.printView(response);

            }).fail(function(response) {
                alert("Error al mostrar la pelicula.");
            });
        },


        printView : function(film){

            var _divRating = this.$el.find('.rating').each(function(index){
                var rating = new vRating({el : $(this), model: film});
                rating.render(film.id);
            });

            var self = this;
            //Pelicula Vista
            var _divFilmVista = this.$el.find('#IdDivPeliculaVista');

            var vista = new vVista({el : _divFilmVista, model: film});
            vista.render(film.id);
            //Pelicula Favorita
            var _divFilmFavorita =  this.$el.find('#IdDivPeliculaFavorita');

            var favorita = new vFavorita({el : _divFilmFavorita, model: film});
            favorita.render(film.id);

            var _divNotaMitja =  this.$el.find('.notaMitja').eq(0);
            var notaMitja = new vNotaMitja({el : _divNotaMitja, model: film});
            notaMitja.render(film.id);

            var _divOpinions = this.$el.find('.opinions').eq(0);
            var opinions = new vOpinions({el : _divOpinions});
            opinions.render(film.id);
        },

        createOpinion : function(){
            var modal = new vCrearOpinio({});
            modal.render(this.model);
        }

    });

    return view;

});
