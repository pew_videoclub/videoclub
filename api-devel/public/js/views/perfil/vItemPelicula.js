/**
 * Created by Gabri on 16/05/2014.
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'bootstrap'
], function($, _, Backbone,Bootstrap){

    var view = Backbone.View.extend({

        events: {
        },

        initialize : function(){
            this.render();
        },

        render: function(){
            this.$el.html("");

            this.$el.addClass('peliculaVista');
            this.$el.append('<div class="col-md-12">' +
                '<div class="col-md-1">' +
                '<a href="#film/'+ this.model.pelicula.id +'">' +
                '<img class="imgItemPelicula" src="img/films/' + this.model.pelicula.cartell +'">' +
                '</a>' +
                '</div>' +
                '<div class="col-md-11 titleItem">' +
                '<a href="#film/'+ this.model.pelicula.id +'">' +
                this.model.pelicula.titol +
                '</a>' +
                '</div>' +
                '</div>');


        },

        renderDate : function(date){

            var daterender = "";
            try{


                var array = date.split("T");

                if(array.length == 2){

                    var arrayDay = array[0].split("-");

                    if(arrayDay.length == 3){
                        daterender += arrayDay[2] +"/"+ arrayDay[1]+"/"+ arrayDay[0];
                    }

                    daterender += " ";

                    var arrayHour = array[1].split(":");

                    if(arrayHour.length > 2){
                        daterender += arrayHour[0] +":"+ arrayHour[1];

                    }
                }

                return daterender;

            }catch (exception){
                return daterender;
            }
        }

    });

    return view;

});