/**
 * Created by Gabri on 16/05/2014.
 */

define([
    'jquery',
    'underscore',
    'backbone',
    'bootstrap',
    'views/perfil/vItemComentari'
    ], function($, _, Backbone,Bootstrap,vItemComentari) {

        var view = Backbone.View.extend({

        events: {
        },

        initialize : function(){
            this.render();
        },

        render: function(userId){
            this.$el.html("");
            var self = this;

            var json = $.ajax({
                url : "/opinions/user/"+userId,
                type : "GET",
                dataType : "json"
            }).done(function( response, status, jqXHR ) {

                self.printList(response);

            }).fail(function(response) {
            });

        },

        printList : function(list){

            if(list == null){return;}
            for(var i in list){
                this.printItem(list[i]);
            }
        },
        printItem : function(item){

            if(item == null){return;}

            this.$el.append('<div class="col-md-12"></div>');
            var _div = this.$el.find('div:last');

            var comentari = new vItemComentari({el: _div, model: item});

        }

    });

    return view;
} );

