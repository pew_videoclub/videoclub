/**
 * Created by marc on 09/05/14.
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'bootstrap'
], function($, _, Backbone,Bootstrap){

    var view = Backbone.View.extend({

        events: {
        },

        initialize : function(){
            this.render();
        },

        render: function(){
            var self = this;
            this.$el.html("");

            this.$el.addClass('activitat');
            this.$el.append('<div class="col-md-12">' +
                this.model.description +
                '</div>' +
                '<div class="col-md-12"> <small>' +
                self.renderDate(this.model.createdAt) +
                '</small></div>');
        },

        renderDate : function(date){

            var daterender = "";
            try{


                var array = date.split("T");

                 if(array.length == 2){

                     var arrayDay = array[0].split("-");

                     if(arrayDay.length == 3){
                         daterender += arrayDay[2] +"/"+ arrayDay[1]+"/"+ arrayDay[0];
                     }

                     daterender += " ";

                     var arrayHour = array[1].split(":");

                     if(arrayHour.length > 2){
                         daterender += arrayHour[0] +":"+ arrayHour[1];

                     }
                 }

                return daterender;

            }catch (exception){
                return daterender;
            }
        }

    });

    return view;

});
