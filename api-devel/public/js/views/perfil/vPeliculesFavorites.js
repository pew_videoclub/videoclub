/**
 * Created by Gabri on 16/05/2014.
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'bootstrap',
    'views/perfil/vItemPelicula'
], function($, _, Backbone,Bootstrap,vItemPelicula){

    var view = Backbone.View.extend({

        events: {
        },

        initialize : function(){
        },

        render: function(userId){
            this.$el.html("");
            var self = this;

            var json = $.ajax({
                url : "/user/favourites/"+userId,
                type : "GET",
                dataType : "json"
            }).done(function( response, status, jqXHR ) {

                self.printList(response);

            }).fail(function(response) {
            });

        },

        printList : function(list){

            if(list == null){return;}
            for(var i in list){
                this.printItem(list[i]);
            }
        },
        printItem : function(item){

            if(item == null){return;}

            this.$el.append('<div class="col-md-12"></div>');
            var _div = this.$el.find('div:last');

            var peliculaFavorita = new vItemPelicula({el: _div, model: item});

        }

    });

    return view;

});
