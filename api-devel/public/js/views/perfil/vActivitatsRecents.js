/**
 * Created by marc on 09/05/14.
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'bootstrap',
    'views/perfil/vItemActivitat'
], function($, _, Backbone,Bootstrap,vItemActivitat){

    var view = Backbone.View.extend({

        events: {
        },

        initialize : function(){
        },

        render: function(userId){
            this.$el.html("");
            var self = this;

            var json = $.ajax({
                url : "/user/activity/"+userId,
                type : "GET",
                dataType : "json"
            }).done(function( response, status, jqXHR ) {

                self.printList(response);

            }).fail(function(response) {
            });


        },

        printList : function(list){

            if(list == null){return;}
            for(var i in list){
                this.printItem(list[i]);
            }
        },
        printItem : function(item){

            if(item == null){return;}

            this.$el.append('<div class="col-md-12"></div>');
            var _div = this.$el.find('div:last');

            var activitat = new vItemActivitat({el: _div, model: item});

        }

    });

    return view;

});
