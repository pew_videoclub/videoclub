/**
 * Created by Gabri on 16/05/2014.
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'bootstrap',
], function($, _, Backbone,Bootstrap,tPerfil,vVar){

    var view = Backbone.View.extend({

        events: {
        },

        initialize : function(){
            this.render();
        },

        render: function(){
            this.$el.html("");

            this.$el.addClass('comentari');
            this.$el.append('<div><strong>'+this.model.pelicula.titol+'</strong></div>' +
                ' <div class="col-md-12">' +
                this.model.opinion +
                '</div>');
        }

    });

    return view;

});