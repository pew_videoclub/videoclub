/**
 * Created by marc on 24/04/14.
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'bootstrap',
    'text!../../templates/perfil/tPerfil.html',
    'views/vVar',
    'views/perfil/vActivitatsRecents',
    'views/perfil/vComentaris',
    'views/perfil/vPeliculesVistes',
    'views/perfil/vPeliculesFavorites'
], function($, _, Backbone,Bootstrap,tPerfil,vVar,vActivitatsRecents,vComentaris,vPeliculesVistes,vPeliculesFavorites){

    var view = Backbone.View.extend({

        el: $('#contextApp'),

        events: {
            "click #btnActivitatsRecents" : "btnActivitatsRecents",
            "click #btnComentaris" : "btnComentaris",
            "click #btnPeliculesVistes" : "btnPeliculesVistes",
            "click #btnPeliculesFavorites" : "btnPeliculesFavorites"
        },

        initialize : function(){
            $('#varApp').html("");
            this.$el.html("");
        },

        render: function(userId){
            $('#varApp').html("");
            this.$el.html("");

             var _vVar = new vVar();
             this.$el.append( _.template( tPerfil, {} ) );


            var activitatsRecents = new vActivitatsRecents({el : this.$el.find('#IdPerfilActivitatsRecents')});
            activitatsRecents.render(userId);

            var comentaris = new vComentaris({el : this.$el.find('#IdPerfilComentaris')});
            comentaris.render(userId);
            this.$el.find('#IdPerfilComentaris').hide();

            var peliculesVistes = new vPeliculesVistes({el : this.$el.find('#IdPerfilPeliculesVistes')});
            peliculesVistes.render(userId);
            this.$el.find('#IdPerfilPeliculesVistes').hide();

            var peliculesFavorites = new vPeliculesFavorites({el : this.$el.find('#IdPerfilPeliculesFavorites')});
            peliculesFavorites.render(userId);
            this.$el.find('#IdPerfilPeliculesFavorites').hide();

        },

        btnActivitatsRecents : function(){
            this.$el.find('#IdPerfilActivitatsRecents').show();
            this.$el.find('#IdPerfilComentaris').hide();
            this.$el.find('#IdPerfilPeliculesVistes').hide();
            this.$el.find('#IdPerfilPeliculesFavorites').hide();
            this.$el.find('#btnActivitatsRecents').addClass('activePerfil');
            this.$el.find('#btnComentaris').removeClass('activePerfil');
            this.$el.find('#btnPeliculesVistes').removeClass('activePerfil');
            this.$el.find('#btnPeliculesFavorites').removeClass('activePerfil');
        },
        btnComentaris : function(){
            this.$el.find('#IdPerfilActivitatsRecents').hide();
            this.$el.find('#IdPerfilComentaris').show();
            this.$el.find('#IdPerfilPeliculesVistes').hide();
            this.$el.find('#IdPerfilPeliculesFavorites').hide();
            this.$el.find('#btnActivitatsRecents').removeClass('activePerfil');
            this.$el.find('#btnComentaris').addClass('activePerfil');
            this.$el.find('#btnPeliculesVistes').removeClass('activePerfil');
            this.$el.find('#btnPeliculesFavorites').removeClass('activePerfil');
        },
        btnPeliculesVistes : function(){
            this.$el.find('#IdPerfilActivitatsRecents').hide();
            this.$el.find('#IdPerfilComentaris').hide();
            this.$el.find('#IdPerfilPeliculesVistes').show();
            this.$el.find('#IdPerfilPeliculesFavorites').hide();
            this.$el.find('#btnActivitatsRecents').removeClass('activePerfil');
            this.$el.find('#btnComentaris').removeClass('activePerfil');
            this.$el.find('#btnPeliculesVistes').addClass('activePerfil');
            this.$el.find('#btnPeliculesFavorites').removeClass('activePerfil');
        },
        btnPeliculesFavorites : function(){
            this.$el.find('#IdPerfilActivitatsRecents').hide();
            this.$el.find('#IdPerfilComentaris').hide();
            this.$el.find('#IdPerfilPeliculesVistes').hide();
            this.$el.find('#IdPerfilPeliculesFavorites').show();
            this.$el.find('#btnActivitatsRecents').removeClass('activePerfil');
            this.$el.find('#btnComentaris').removeClass('activePerfil');
            this.$el.find('#btnPeliculesVistes').removeClass('activePerfil');
            this.$el.find('#btnPeliculesFavorites').addClass('activePerfil');
        }

    });

    return view;

});
