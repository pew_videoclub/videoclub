// Filename: main.js
require.config({
    paths: {
        jquery: 'libs/jquery/jquery-2.1.0.min',
        underscore: 'libs/underscore/underscore-min',
        backbone: 'libs/backbone/backbone-min',
        bootstrap: 'libs/bootstrap/bootstrap.min',
        text: 'text',
        utils: 'libs/utils/utils'
    }
});

require(['router'], function(Router){
   var router = new Router;
});