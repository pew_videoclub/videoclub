// Filename: router.js
define([
    'jquery',
    'underscore',
    'backbone',
    'bootstrap',
    'views/vLogin',
    'views/vHome',
    'views/vPerfil',
    'views/vFilm',
    'views/vSearch',
    'views/vNoticies'
], function($, _, Backbone,Bootstrap,vLogin,vHome,vPerfil,vFilm,vSearch,vNoticies){

    var AppRouter = Backbone.Router.extend({
        routes: {
            "" : "showLogin",
            "perfil": "showProfileSelf",
            "perfil/:id": "showProfile",
            "search/:text": "showSearch",
            "film/:id": "showFilm",
            "home": "showHome",
            "login": "showLogin"
        },

        initialize : function(){
            var login = new vLogin();
        },

        showLogin : function(){
            var login = new vLogin();
        },

        showHome : function(){
            var home = new vHome();
        },

        showSearch : function(text){
            var search = new vSearch();
            search.render(text);
        },

        showProfileSelf : function(){
            var perfil = new vPerfil();
            perfil.render(id);
        },

        showProfile : function(id){
            var perfil = new vPerfil();
            perfil.render(id);
        },

        showFilm : function(id){
            var film = new vFilm();
            film.render(id);
        },
        showNews : function(){
            var news = new vNoticies();
        }
    });

    Backbone.history.start();

    return AppRouter;
});