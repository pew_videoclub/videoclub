/**
 * New node file
 */

module.exports = function(sequelize, DataTypes) {
	var Activity = sequelize.define('Activity', {
		description	: DataTypes.STRING
	}, {
		classMethods : {
			associate : function(models) {
                Activity.belongsTo(models.User)
                Activity.belongsTo(models.Pelicula)
			}
		}
	});

	return Activity;
};
