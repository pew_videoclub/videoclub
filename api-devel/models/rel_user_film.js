



module.exports = function(sequelize, DataTypes) {
    var RelUserFilm = sequelize.define('RelUserFilm', {
        nota : DataTypes.INTEGER,
        vista : DataTypes.BOOLEAN,
        favorita : DataTypes.BOOLEAN
    }, {
        classMethods : {
            associate : function(models) {
                RelUserFilm.belongsTo(models.Pelicula)
                RelUserFilm.belongsTo(models.User)
            }
        }
    });

    return RelUserFilm;
};