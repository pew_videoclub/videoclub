/**
 * New node file
 */

module.exports = function(sequelize, DataTypes) {

	var User = sequelize.define('User', {
		name : DataTypes.STRING(255),
		surname : DataTypes.STRING(255),
		address : DataTypes.STRING(512),
		city : DataTypes.STRING(255),
		country : DataTypes.STRING(255),
		born_date : DataTypes.DATE,
		sex : DataTypes.STRING(1),
        email : DataTypes.STRING(255),
        pwd : DataTypes.STRING(30)

	}, {
		classMethods : {

			associate : function(models) {
                User.hasMany(models.Opinion)
                User.hasMany(models.RelUserFilm)
            }
		}
	});
	
	return User;
};