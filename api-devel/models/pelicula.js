/**
 * New node file
 */

module.exports = function(sequelize, DataTypes) {	
	var Pelicula = sequelize.define('Pelicula', {
		titol : DataTypes.STRING(50),
		titol_original : DataTypes.STRING(50),
		sinapsi : DataTypes.TEXT,
        cartell : DataTypes.STRING(50)
	}, {
		classMethods : {
			associate : function(models) {
                Pelicula.belongsTo(models.Productora)
                Pelicula.hasMany(models.Opinion)
                Pelicula.hasMany(models.RelUserFilm)
            }
		}
	});
	
	return Pelicula;
};