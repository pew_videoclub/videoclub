/**
 * Created by marc on 03/04/14.
 */

module.exports = function(sequelize, DataTypes) {
    var Productora = sequelize.define('Productora', {
        name : DataTypes.STRING(255),
        country : DataTypes.STRING(255)

    }, {
        classMethods : {
            associate : function(models) {
                Productora.hasMany(models.Pelicula,{as: 'Pelicules'})
            }
        }
    });

    return Productora;
};