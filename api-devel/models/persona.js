/**
 * Created by marc on 03/04/14.
 */

module.exports = function(sequelize, DataTypes) {
    var Persona = sequelize.define('Persona', {
        name : DataTypes.STRING(255),
        surname : DataTypes.STRING(255),
        country : DataTypes.STRING(255),
        born_date : DataTypes.DATE
    }, {
        classMethods : {
            associate : function(models) {
            }
        }
    });

    return Persona;
};