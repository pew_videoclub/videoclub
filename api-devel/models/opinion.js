/**
 * New node file
 */

module.exports = function(sequelize, DataTypes) {
	var Opinion = sequelize.define('Opinion', {
		opinion	: DataTypes.STRING
	}, {
		classMethods : {
			associate : function(models) {
                Opinion.belongsTo(models.Pelicula)
                Opinion.belongsTo(models.User)
			}
		}
	});

	return Opinion;
};
