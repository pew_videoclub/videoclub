
/*
 * GET users listing.
 */

module.exports = function (db){

    var uuid = require('node-uuid');
    var util = require('../util');
    var dao = require('../dao')(db);

    return {
        login: function (req, res) {
            if (!req.body.mail) {
                util.stdErr400(res, "Missing 'email' attribute");
                return;
            } else var mail = req.body.mail;
            if (!req.body.pwd) {
                util.stdErr400(res, "Missing 'email' attribute");
                return;
            } else var pwd = req.body.pwd;
            var json;
            db.sequelize.transaction(function (t) {
                dao.User.getByEmail(mail)
                    .then(function (user) {
                        if (user) {
                            if (user.pwd != pwd) {
                                util.reject("Contrasenya incorrecta");
                            }else{
                                req.session.userID = user.id;
                                return user;
                            }

                        } else {
                            util.reject("eMail no registrat");
                        }
                    }).then(util.commit.genFuncLeft(t), util.rollback.genFuncLeft(t))
                    .then(util.stdSeqSuccess.genFuncLeft(res), util.stdSeqError.genFuncLeft(res))
                    .done();
            });
        },
        logout: function (req,res) {
            if (req.session.userID) {
                delete req.session.userID
                util.stdSeqSuccess(res)
            } else {
                util.stdErr400(res,'No loguejat');
            };
        },
        create: function (req, res) {
            var self = this;
            if (!req.body.mail) {
                util.stdErr400(res, "Camp de correu buit");
                return;
            } else var email = req.body.mail;
            if (!req.body.name) {
                util.stdErr400(res, "Camp de nom buit");
                return;
            } else var name = req.body.name;
            if (!req.body.cognom) {
                util.stdErr400(res, "Camp de cognom buit");
                return;
            } else var surname = req.body.cognom;
            if (!req.body.pass1 || !req.body.pass2) {
                util.stdErr400(res, "Camps contrasenya buits");
                return;
            } else if (req.body.pass1 != req.body.pass2) {
                util.stdErr400(res, "Contrasenyes no iguals");
                return;
            } else var pwd = req.body.pass1;

            var attribs= {name: name, surname: surname,email: email, pwd: pwd}

            db.sequelize.transaction(function (t) {
            dao.User.getByEmail(email)
                .then(function (user) {
                    if (user) {
                        util.reject("Already exist a user with email");
                    } else {

                        dao.User.create(attribs).then(function(u){
                            dao.User.getByEmail(email)
                                .then(function (user) {
                                    if (user) {
                                        var id = user.id;
                                        req.session.userID = id;
                                        return user;
                                    } else {
                                        util.reject("error auth");
                                    }
                                }).then(util.commit.genFuncLeft(t), util.rollback.genFuncLeft(t))
                                .then(util.stdSeqSuccess.genFuncLeft(res), util.stdSeqError.genFuncLeft(res))
                                .done();
                        });
                    }
                });
            });
        },
        info: function (req, res) {
            //db.Client.find(req.params.id).success(function(client){
            //res.setHeader('Content-Type','text-json');
            //res.end(JSON.stringify(client));
            //});
            //
            if (!req.sessionID) {
                util.stdErr400(res, "Please login for info");
                return;
            }
            if (!req.params.id) {
                util.stdErr400(res, "Missing 'id' attribute");
                return;
            } else var id = req.params.id;
            db.sequelize.transaction(function (t) {
                dao.User.getById(id)
                    .then(function (user) {
                        if (user) {
                            return user;
                        }
                        else {
                            util.reject("User doesn't exist");
                        }
                    }).then(util.commit.genFuncLeft(t), util.rollback.genFuncLeft(t))
                    .then(util.stdSeqSuccess.genFuncLeft(res), util.stdSeqError.genFuncLeft(res))
                    .done();
            });
        },
        getFavorites: function (req, res) {
            if (!req.session.userID) {
                util.stdErr400(res, 'No loguejat');
                return;
            } else if (!req.params.userId) {
                util.stdErr400(res, "Missing 'id' attribute");
                return;
            } else var userId = req.params.userId;

            db.sequelize.transaction(function (t) {
                dao.RelUserFilm.getFavorites(userId)
                    .then(function (llistat) {
                        if (llistat) return llistat;
                        else util.reject("Error al llistar");
                    }).then(util.commit.genFuncLeft(t), util.rollback.genFuncLeft(t))
                    .then(util.stdSeqSuccess.genFuncLeft(res), util.stdSeqError.genFuncLeft(res))
                    .done();

            });
        },
        getVieweds: function (req, res) {
            if (!req.session.userID) {
                util.stdErr400(res, 'No loguejat');
                return;
            } else if (!req.params.userId) {
                util.stdErr400(res, "Missing 'id' attribute");
                return;
            } else var userId = req.params.userId;

            db.sequelize.transaction(function (t) {
                dao.RelUserFilm.getVieweds(userId)
                    .then(function (llistat) {
                        if (llistat) return llistat;
                        else util.reject("Error al llistar");
                    }).then(util.commit.genFuncLeft(t), util.rollback.genFuncLeft(t))
                    .then(util.stdSeqSuccess.genFuncLeft(res), util.stdSeqError.genFuncLeft(res))
                    .done();

            });
        },

        getActivity: function (req, res) {
            if (!req.session.userID) {
                util.stdErr400(res, 'No loguejat');
                return;
            } else if (!req.params.userId) {
                util.stdErr400(res, "Missing 'id' attribute");
                return;
            } else var userId = req.params.userId;

            db.sequelize.transaction(function (t) {
                dao.Activity.listActivity(userId)
                    .then(function (llistat) {
                        if (llistat) return llistat;
                        else util.reject("Error al llistar");
                    }).then(util.commit.genFuncLeft(t), util.rollback.genFuncLeft(t))
                    .then(util.stdSeqSuccess.genFuncLeft(res), util.stdSeqError.genFuncLeft(res))
                    .done();

            });
        },

        getMe: function (req, res) {

            if (!req.session.userID) {
                util.stdErr400(res, 'No loguejat');
                return;
            } else var userId = req.session.userID;

            db.sequelize.transaction(function (t) {
                dao.User.getById(userId)
                    .then(function (user) {
                        if (user) {
                            return user;
                        }
                        else {
                            util.reject("User doesn't exist");
                        }
                    }).then(util.commit.genFuncLeft(t), util.rollback.genFuncLeft(t))
                    .then(util.stdSeqSuccess.genFuncLeft(res), util.stdSeqError.genFuncLeft(res))
                    .done();
            });
        }
    }
};