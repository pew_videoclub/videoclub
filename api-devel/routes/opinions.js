/*
 * GET users listing.
 */

module.exports = function(db) {

	var util = require('../util');
	var dao = require('../dao')(db);

	return {
        getOpinion: function (req, res) {
            if (!req.session.userID) {
                util.stdErr400(res, 'No loguejat');
                return;
            } else var userId = req.session.userID;

            if (!req.params.id) {
                util.stdErr400(res, "Cerca buida");
                return;
            } else var opinionId = req.params.id;

            db.sequelize.transaction(function (t) {
                dao.Opinion.getOpinion(opinionId)
                    .then(function (opinion){
                        return opinion;
                    }).then(util.commit.genFuncLeft(t), util.rollback.genFuncLeft(t))
                    .then(util.stdSeqSuccess.genFuncLeft(res), util.stdSeqError.genFuncLeft(res))
                    .done();
            });
        },

        editOpinion: function (req, res) {
            if (!req.session.userID) {
                util.stdErr400(res, 'No loguejat');
                return;
            } else var userId = req.session.userID;

            if (!req.params.id) {
                util.stdErr400(res, "Missing 'id' attribute in route");
                return;
            } else var opinionId = req.params.id;

            if (!req.body.opinion) {
                util.stdErr400(res, "Sense el camp 'opinio'");
                return;
            } else var opinion = req.body.opinion;

            db.sequelize.transaction(function (t) {
                dao.Opinion.getOpinion(opinionId)
                    .then(function (opinionOld){
                        opinionOld.updateAttributes({opinion: opinion})
                        return opinionOld;
                    }).then(util.commit.genFuncLeft(t), util.rollback.genFuncLeft(t))
                    .then(util.stdSeqSuccess.genFuncLeft(res), util.stdSeqError.genFuncLeft(res))
                    .done();
            });
        },

        listOpinions: function (req, res) {
            if (!req.session.userID) {
                util.stdErr400(res, 'No loguejat');
                return;
            } else var userId = req.session.userID;

            if (!req.params.filmId) {
                util.stdErr400(res, "Cerca buida");
                return;
            } else var filmId = req.params.filmId;

            db.sequelize.transaction(function (t) {
                dao.Opinion.listOpinions(filmId)
                    .then(function (llistat) {
                        if (llistat) return llistat;
                        else util.reject("Error al llistar");
                    }).then(util.commit.genFuncLeft(t), util.rollback.genFuncLeft(t))
                    .then(util.stdSeqSuccess.genFuncLeft(res), util.stdSeqError.genFuncLeft(res))
                    .done();

            });
        },

        listOpinionsByUserId: function (req, res) {
            if (!req.session.userID) {
                util.stdErr400(res, 'No loguejat');
                return;
            } else var userId = req.session.userID;

            if (!req.params.user_Id) {
                util.stdErr400(res, "Cerca buida");
                return;
            } else var user_Id = req.params.user_Id;

            db.sequelize.transaction(function (t) {
                dao.Opinion.listOpinionsByUserId(user_Id)
                    .then(function (llistat) {
                        if (llistat) return llistat;
                        else util.reject("Error al llistar");
                    }).then(util.commit.genFuncLeft(t), util.rollback.genFuncLeft(t))
                    .then(util.stdSeqSuccess.genFuncLeft(res), util.stdSeqError.genFuncLeft(res))
                    .done();

            });
        },

		create : function(req,res) {
            if (!req.session.userID) {
                util.stdErr400(res, "Please login for info");
                return;
            } else var userId = req.session.userID;

            if (!req.params.filmId) {
                util.stdErr400(res, "Missing 'filmId' attribute in route");
                return;
            } else var filmId = req.params.filmId;

            if (!req.body.opinion) {
                util.stdErr400(res, "Missing 'opinion' attribute in body");
                return;
            } var opinion = req.body.opinion;

            db.sequelize.transaction(function (t) {
                dao.Opinion.create(filmId,userId,opinion)
                    .then(function (opinion) {
                        if (!opinion) {
                            util.stdErr400(res, "Impossible registrar l'opinio");
                        } else {
                            dao.Activity.create("Ha escrit una nova opinio",userId,filmId)
                            return opinion;
                        }
                    })
                    .then(util.commit.genFuncLeft(t), util.rollback.genFuncLeft(t))
                    .then(util.stdSeqSuccess.genFuncLeft(res), util.stdErr500.genFuncLeft(res))
                    .done();
            });
		}
	}
}
