module.exports = function (db){

    var uuid = require('node-uuid');
    var util = require('../util');
    var dao = require('../dao')(db);

    return {
        getFilm: function (req, res) {
            if (!req.session.userID) {
                util.stdErr400(res, 'No loguejat')
                return;
            } else var userId = req.session.userID;

            if (!req.params.filmId) {
                util.stdErr400(res, "Cerca buida");
                return;
            } else var filmId = req.params.filmId;

            db.sequelize.transaction(function (t) {
                dao.Pelicula.getById(filmId)
                    .then(function (film) {
                        if (film) return film;
                        else util.reject("Cap pel·licula coincideix amb els parametres de cerca: " + filmId);
                    }).then(util.commit.genFuncLeft(t), util.rollback.genFuncLeft(t))
                    .then(util.stdSeqSuccess.genFuncLeft(res), util.stdSeqError.genFuncLeft(res))
                    .done();
            });
        },

        getNotaMitja: function (req, res) {
            if (!req.session.userID) {
                util.stdErr400(res, 'No loguejat')
                return;
            } else var userId = req.session.userID;

            if (!req.params.filmId) {
                util.stdErr400(res, 'Parametre filmId buit');
                return;
            } else var filmId = req.params.filmId;

            db.sequelize.transaction(function (t) {
                dao.RelUserFilm.getNotaMitja(filmId)
                    .then(function (list){

                        // Aixó és te que arreglar amb la query.
                        if(!list || list.length == 0){
                            return false;
                        }
                        var nMitja = 0;

                        for(var i in list){
                            nMitja += list[i].nota;
                        }
                        return {nota : nMitja/list.length};

                    }).then(util.commit.genFuncLeft(t), util.rollback.genFuncLeft(t))
                    .then(util.stdSeqSuccess.genFuncLeft(res), util.stdSeqError.genFuncLeft(res))
                    .done();
            });
        },

        getRating: function (req, res) {
            if (!req.session.userID) {
                util.stdErr400(res, 'No loguejat')
                return;
            } else var userId = req.session.userID;

            if (!req.params.filmId) {
                util.stdErr400(res, 'Parametre filmId buit');
                return;
            } else var filmId = req.params.filmId;

            db.sequelize.transaction(function (t) {
                dao.RelUserFilm.getRating(filmId,userId)
                    .then(function (nota){
                        if (!nota) {
                            return dao.RelUserFilm.create(filmId,userId)
                        } else {
                            return nota;
                        }
                    }).then(util.commit.genFuncLeft(t), util.rollback.genFuncLeft(t))
                    .then(util.stdSeqSuccess.genFuncLeft(res), util.stdSeqError.genFuncLeft(res))
                    .done();
            });
        },

        setRating: function (req, res) {
            if (!req.session.userID) {
                util.stdErr400(res, 'No loguejat');
                return;
            } else var userId = req.session.userID;

            if (!req.params.filmId) {
                util.stdErr400(res, "Cerca buida");
                return;
            } else var filmId = req.params.filmId;

            if (!req.body.nota) {
                util.stdErr400(res, "No hi ha nota");
                return;
            } else var nota = req.body.nota;

            db.sequelize.transaction(function (t) {
                dao.RelUserFilm.getRating(filmId,userId)
                    .then(function (valoracio){
                        if (!valoracio) {
                            valoracio = dao.RelUserFilm.create(filmId,userId);
                        }

                        valoracio.updateAttributes({nota: nota});

                        dao.Pelicula.getById(filmId).then(function (pelicula) {
                            dao.Activity.create("Ha puntuat la pelicula "+ pelicula.titol +" amb un " + nota,userId,filmId)


                        });
                        return valoracio;

                    }).then(util.commit.genFuncLeft(t), util.rollback.genFuncLeft(t))
                    .then(util.stdSeqSuccess.genFuncLeft(res), util.stdSeqError.genFuncLeft(res))
                    .done();
            });
        },

        getVista: function (req, res) {
            if (!req.session.userID) {
                util.stdErr400(res, 'No loguejat');
                return;
            } else var userId = req.session.userID;

            if (!req.params.filmId) {
                util.stdErr400(res, "Cerca buida");
                return;
            } else var filmId = req.params.filmId;

            db.sequelize.transaction(function (t) {
                dao.RelUserFilm.getVista(filmId,userId)
                    .then(function (vista){
                        return vista;
                    }).then(util.commit.genFuncLeft(t), util.rollback.genFuncLeft(t))
                    .then(util.stdSeqSuccess.genFuncLeft(res), util.stdSeqError.genFuncLeft(res))
                    .done();
            });
        },

        setVista: function (req, res) {
            if (!req.session.userID) {
                util.stdErr400(res, 'No loguejat');
                return;
            } else var userId = req.session.userID;

            if (!req.params.filmId) {
                util.stdErr400(res, "Cerca buida");
                return;
            } else var filmId = req.params.filmId;

            db.sequelize.transaction(function (t) {
                dao.RelUserFilm.getVista(filmId,userId)
                    .then(function (vista){
                        if (!vista) {
                            vista = dao.RelUserFilm.create(filmId,userId);
                        }
                        vista.updateAttributes({vista: true});
                        dao.Pelicula.getById(filmId).then(function (pelicula) {

                            dao.Activity.create("Ha marcat com a vista "+ pelicula.titol,userId,filmId);
                            return vista;
                        });

                    }).then(util.commit.genFuncLeft(t), util.rollback.genFuncLeft(t))
                    .then(util.stdSeqSuccess.genFuncLeft(res), util.stdSeqError.genFuncLeft(res))
                    .done();
            });
        },

        unsetVista: function (req, res) {
            if (!req.session.userID) {
                util.stdErr400(res, 'No loguejat');
                return;
            } else var userId = req.session.userID;

            if (!req.params.filmId) {
                util.stdErr400(res, "Cerca buida");
                return;
            } else var filmId = req.params.filmId;

            db.sequelize.transaction(function (t) {
                dao.RelUserFilm.getVista(filmId,userId)
                    .then(function (vista){
                        vista.updateAttributes({vista: false});
                        dao.Pelicula.getById(filmId).then(function (pelicula) {

                            dao.Activity.create("Ha desmarcat com a vista "+ pelicula.titol,userId,filmId);
                            return vista;
                        });
                    }).then(util.commit.genFuncLeft(t), util.rollback.genFuncLeft(t))
                    .then(util.stdSeqSuccess.genFuncLeft(res), util.stdSeqError.genFuncLeft(res))
                    .done();
            });
        },

        getFavorita: function (req, res) {
            if (!req.session.userID) {
                util.stdErr400(res, 'No loguejat');
                return;
            } else var userId = req.session.userID;

            if (!req.params.filmId) {
                util.stdErr400(res, "Cerca buida");
                return;
            } else var filmId = req.params.filmId;

            db.sequelize.transaction(function (t) {
                dao.RelUserFilm.getFavorita(filmId,userId)
                    .then(function (favorita){
                        return favorita;
                    }).then(util.commit.genFuncLeft(t), util.rollback.genFuncLeft(t))
                    .then(util.stdSeqSuccess.genFuncLeft(res), util.stdSeqError.genFuncLeft(res))
                    .done();
            });
        },

        setFavorita: function (req, res) {
            if (!req.session.userID) {
                util.stdErr400(res, 'No loguejat');
                return;
            } else var userId = req.session.userID;

            if (!req.params.filmId) {
                util.stdErr400(res, "Cerca buida");
                return;
            } else var filmId = req.params.filmId;

            db.sequelize.transaction(function (t) {
                dao.RelUserFilm.getFavorita(filmId,userId)
                    .then(function (favorita){
                        if (!favorita) {
                            favorita = dao.RelUserFilm.create(filmId,userId);
                        }
                        favorita.updateAttributes({favorita: true});

                        dao.Pelicula.getById(filmId).then(function (pelicula) {

                            dao.Activity.create("Ha marcat com a favorita " + pelicula.titol,userId,filmId);
                            return favorita;
                        });

                    }).then(util.commit.genFuncLeft(t), util.rollback.genFuncLeft(t))
                    .then(util.stdSeqSuccess.genFuncLeft(res), util.stdSeqError.genFuncLeft(res))
                    .done();
            });
        },

        unsetFavorita: function (req, res) {
            if (!req.session.userID) {
                util.stdErr400(res, 'No loguejat');
                return;
            } else var userId = req.session.userID;

            if (!req.params.filmId) {
                util.stdErr400(res, "Cerca buida");
                return;
            } else var filmId = req.params.filmId;

            db.sequelize.transaction(function (t) {
                dao.RelUserFilm.getFavorita(filmId,userId)
                    .then(function (favorita){
                        favorita.updateAttributes({favorita: false});
                        dao.Pelicula.getById(filmId).then(function (pelicula) {

                            dao.Activity.create("Ha desmarcat com a favorita " + pelicula.titol,userId,filmId);
                            return favorita;
                        });
                    }).then(util.commit.genFuncLeft(t), util.rollback.genFuncLeft(t))
                    .then(util.stdSeqSuccess.genFuncLeft(res), util.stdSeqError.genFuncLeft(res))
                    .done();
            });
        },

        search: function (req, res) {
            if (!req.session.userID) {
                util.stdErr400(res, 'No loguejat');
                return;
            } else var userId = req.session.userID;

            if (!req.params.toSearch) {
                util.stdErr400(res, "Cerca buida");
                return;
            } else var busqueda = req.params.toSearch;

            db.sequelize.transaction(function (t) {
                dao.Pelicula.search(busqueda)
                    .then(function (llistat) {
                        if (llistat) return llistat;
                        else util.reject("Cap pel·licula coincideix amb els parametres de cerca: " + busqueda);
                    }).then(util.commit.genFuncLeft(t), util.rollback.genFuncLeft(t))
                    .then(util.stdSeqSuccess.genFuncLeft(res), util.stdSeqError.genFuncLeft(res))
                    .done();
            });
        },

        topVistes: function (req, res) {
            if (!req.session.userID) {
                util.stdErr400(res, 'No loguejat');
                return;
            } else var userId = req.session.userID;

            db.sequelize.transaction(function (t) {
                dao.Pelicula.topVistes()
                    .then(function (llistat) {
                        if (llistat) return llistat;
                        else util.reject("Error al llistar");
                    }).then(util.commit.genFuncLeft(t), util.rollback.genFuncLeft(t))
                    .then(util.stdSeqSuccess.genFuncLeft(res), util.stdSeqError.genFuncLeft(res))
                    .done();

            });
        },

        topBuscades: function (req, res) {
            if (!req.session.userID) {
                util.stdErr400(res, 'No loguejat');
                return;
            } else var userId = req.session.userID;

            db.sequelize.transaction(function (t) {
                dao.Pelicula.topBucades()
                    .then(function (llistat) {
                        if (llistat) {
                            return llistat;
                        }
                        else {
                            util.reject("Error al llistar");
                        }
                    }).then(util.commit.genFuncLeft(t), util.rollback.genFuncLeft(t))
                    .then(util.stdSeqSuccess.genFuncLeft(res), util.stdSeqError.genFuncLeft(res))
                    .done();

            });
        }
    }
};