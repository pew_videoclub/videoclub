var express = require('express')
    , http = require('http')
    , path = require('path')
    , fs = require('fs');
var cors = require('cors');


var app = express()

app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.cookieParser());
app.use(express.session({secret: '1234567890QWERTY'}));
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(cors());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public/')));


Function.prototype.genFuncLeft = function () {
    var fn = this, args = Array.prototype.slice.call(arguments);
    return function () {
        return fn.apply(null, args.concat(Array.prototype.slice.call(arguments)));
    }
}

Function.prototype.genFuncRight = function () {
    var fn = this, args = Array.prototype.slice.call(arguments);
    return function () {
        return fn.apply(null, Array.prototype.slice.call(arguments).concat(args));
    }
}

//Comment

var db = require('./models')(app);

db.initPromise
    .then(function () {
        var opinions = require('./routes/opinions')(db);
        var user = require('./routes/user')(db);
        var pelicula = require('./routes/pelicula')(db);

        //USER
        app.post('/login', user.login);
        app.post('/logout', user.logout);
        app.post('/user', user.create);
        app.get('/user/auth', user.getMe);
        app.get('/user/favourites/:userId', user.getFavorites);
        app.get('/user/vieweds/:userId', user.getVieweds);
        app.get('/user/activity/:userId', user.getActivity);
        app.get('/user/:id', user.info);


        //OPINIONS
        app.get('/opinions/:id', opinions.getOpinion);
        app.put('/opinions/:id', opinions.editOpinion);
        app.get('/opinions/user/:user_Id', opinions.listOpinionsByUserId);

        //PELICULA
        app.get('/pelicula/search/:toSearch', pelicula.search);

        app.get('/pelicula/top/vistes', pelicula.topVistes);
        app.get('/pelicula/top/buscades', pelicula.topBuscades);

        app.get('/pelicula/notaMitja/:filmId', pelicula.getNotaMitja);

        app.get('/pelicula/rating/:filmId', pelicula.getRating);
        app.put('/pelicula/rating/:filmId', pelicula.setRating);

        app.get('/pelicula/vista/:filmId', pelicula.getVista);
        app.post('/pelicula/vista/:filmId', pelicula.setVista);
        app.put('/pelicula/vista/:filmId', pelicula.unsetVista);

        app.get('/pelicula/favorita/:filmId', pelicula.getFavorita);
        app.post('/pelicula/favorita/:filmId', pelicula.setFavorita);
        app.put('/pelicula/favorita/:filmId', pelicula.unsetFavorita);

        app.get('/pelicula/:filmId', pelicula.getFilm);
        app.get('/pelicula/:filmId/getOpinions', opinions.listOpinions)
        app.post('/pelicula/:filmId/opinion', opinions.create);

        var port = process.env.OPENSHIFT_NODEJS_PORT || app.get('port');
        var ip = process.env.OPENSHIFT_NODEJS_IP || "127.0.0.1";

        http.createServer(app).listen(port, ip, function () {
            console.log("Express server listening on " + ip + ":" + port);
        })

    }, function (err) {
        console.log("Error initializing database: " + err);
    })
    .done();;